plot_etalon_dists <- function(data, factors, factor_cols, factor_legend, ...) {
  unique_factors <- sort(unique(factors))
  
  max_x <- 0
  max_y <- 0
  skipped_facs <- NULL
  for (fac in unique_factors) {
    mask <- factors == fac
    if (sum(mask) < 2) {
      skipped_facs <- c(skipped_facs, fac)
      next()
    }
    subset_dens <- density(data[mask,]$EtalonDistance)
    if (max(subset_dens$x) > max_x) max_x <- max(subset_dens$x)
    if (max(subset_dens$y) > max_y) max_y <- max(subset_dens$y)
  }
  
  plot(1, type="n", xlab="Distance", ylab="Probability Density", xlim=c(0, max_x), ylim=c(0, max_y), ...)
  
  for (fac in unique_factors) {
    if (fac %in% skipped_facs) {
      next()
    }
    mask <- factors == fac
    subset_dens <- density(data[mask,]$EtalonDistance)
    lines(subset_dens, col = factor_cols[as.numeric(fac)])
  }
  
  legend(0.1 * max_x, 0.9 * max_y, lty=c(1,1), lwd=c(2.5,2.5), col=factor_cols, legend=factor_legend)
}